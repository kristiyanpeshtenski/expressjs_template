const User = require('../models/user');
const encription = require('../utils/encription');

module.exports = {
    loginGet: (req, res) => {
        res.render('user/login');
    },
    loginPost: (req, res) => {
        let reqUser = req.body;
        User.findOne({ username: reqUser.username }).then((user) => {
            if(!user) {
                console.warn('invalid credentials');
                res.locals.errorMsg = 'Wron username or password'
                res.render('user/login', reqUser);
                return;
            }
            if(!user.authenticate(reqUser.password)){
                console.warn('invalid credentials');
                res.locals.errorMsg = 'Wron username or password'
                res.render('user/login', reqUser);
                return;
            }
            req.logIn(user, (err, user) => {
                if(err) {
                    console.warn(err);
                    res.locals.errorMsg = 'Wrong username or password'
                    res.render('user/login', reqUser);
                    return;
                }

                res.redirect('/');
            });
        })
        .catch((err) => {
            console.warn(err);
        });
    },

    registerGet: (req, res) => {
        res.render('user/register')
    },
    registerPost: (req, res) => {
        let reqUser = req.body;
        if (reqUser.password !== reqUser.confirmedPassword) {
            res.render('user/register', { user: reqUser, error: 'Passwords do not match!' });
            return;
        }
        User.findOne({ username: reqUser.username }).then((user) => {
            if(user) {
                res.render('user/register', { user: reqUser, error: 'Username already exists.' })
                return;
            }
            let salt = encription.generateSalt(); 
            let hashedPass = encription.generateHashedPassword(salt, reqUser.password);
            User.create({
                username: reqUser.username,
                firstName: reqUser.firstName,
                lastName: reqUser.lastName,
                salt: salt,
                password: hashedPass
            })
            .then((user) => {
                req.logIn(user, (err, user) => {
                    if(err) {
                        res.locals.errorMsg = err;
                        res.render('user/register', reqUser);
                        return;
                    }
                    res.redirect('/');
                });
            });
        });
    },
    
    logout: (req, res) => {
        req.logout();
        res.redirect('/');
    }
}