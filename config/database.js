const mongoose = require('mongoose');

mongoose.Promise = global.Promise;
module.exports = (config) => {
    mongoose.connect(config.connectionString);
    let db = mongoose.connection;
    db.once('open', (err) => {
        if(err) throw err;
        console.log('Connected!');
        require('../models/user').seedAdminUser()
    });
    db.on('error', (err) => {
        console.warn('Database error: ' + err);
    });
}