module.exports = {
    isAuthenticated: (req, res, next) => {
        if(req.isAuthenticated()) {
            next();
        } else{
            res.redirect('/login');
        }
    },
    isInRole: (role) => {
        return (req, res, next) => {
            if (req.isAuthenticated() && req.user.roles.indexOf(role) >= 0) {
                next();
            } else{
                res.redirect('/login');
            }
        }
    }
}