const path = require('path');
let port = process.env.port || 3000;
let rootPath = path.normalize(path.join(__dirname, '/../../'));

module.exports = {
    development: {
        connectionString: 'mongodb://localhost:27017/ExpressJs-Template',
        port: port,
        rootPath: rootPath
    },
    production: {

    }
}