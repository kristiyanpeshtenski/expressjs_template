const mongoose = require('mongoose');
const encryption = require('../utils/encription');
const REQUIRED_VALIDATION_MESSAGE = '{PATH} is required';

const userSchema = new mongoose.Schema({
    username: { type: mongoose.Schema.Types.String, required: REQUIRED_VALIDATION_MESSAGE, unique: true },
    firstName: { type: mongoose.Schema.Types.String, required: REQUIRED_VALIDATION_MESSAGE },
    lastName: { type: mongoose.Schema.Types.String, required: REQUIRED_VALIDATION_MESSAGE },
    salt: { type: mongoose.Schema.Types.String, required: true },
    password: { type: mongoose.Schema.Types.String, required: true },
    roles: [{ type: mongoose.Schema.Types.String }]
});

userSchema.method({
    authenticate: function(password) {
        if(encryption.generateHashedPassword(this.salt, password) === this.password) {
            return true;
        } else {
            return false;
        }
    }
});

const User = mongoose.model('User', userSchema);
module.exports = User;
module.exports.seedAdminUser = () => {
    User.find().then((users) => {
        if (users.length > 0) return;
        let salt = encryption.generateSalt();
        let hashedPassword = encryption.generateHashedPassword(salt, 'admin');
        User.create({
            username: 'admin',
            firstName: 'admin',
            lastName: 'admin',
            salt: salt,
            password: hashedPassword,
            roles: ['Admin']
        })
        .then(() => { 
            console.log('Successfully Added admin user');
         })
         .catch((err) => { throw err })
    });
}